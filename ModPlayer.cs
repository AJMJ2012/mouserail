using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using Terraria;
using Terraria.ModLoader;
using Terraria.ModLoader.IO;
using Terraria.GameContent.Events;
using Terraria.ID;
using Terraria.IO;
using Terraria.UI;

namespace MouseRail {
	public class MPlayer : ModPlayer {
		public override void DrawEffects(PlayerDrawInfo drawInfo, ref float r, ref float g, ref float b, ref float a, ref bool fullBright) {
			if (Main.netMode != 2 && !Main.gameMenu && !Main.ingameOptionsWindow) {
				Player player = Main.player[Main.myPlayer];

				Vector2 PlayerCenter = new Vector2(player.Center.X - Main.screenPosition.X, player.Center.Y - Main.screenPosition.Y);
				Vector2 LinePos = new Vector2();

				int max = 96;
				int start = 32;
				float distance = Vector2.Distance(player.Center, Main.MouseWorld);
				float angle = (float)Math.Atan2(player.Center.Y - Main.MouseWorld.Y, player.Center.X - Main.MouseWorld.X);
				if (Config.Client.Method == 1) {
					//distance = Vector2.Distance(player.Center, new Vector2(Main.MouseWorld.X + (Main.cursorTextures[0].Width * 0.5f), Main.MouseWorld.Y + (Main.cursorTextures[0].Height * 0.5f)));
					//angle = (float)Math.Atan2(player.Center.Y - Main.MouseWorld.Y - (Main.cursorTextures[0].Height * 0.5f), player.Center.X - Main.MouseWorld.X - (Main.cursorTextures[0].Width * 0.5f));
					float alpha = 1f;
					if (Main.ThickMouse) {
						for (int index = 0; index < distance - 2; index+=(int)(4 * 0.5f)) {
							alpha = MathHelper.Clamp(((float)(index - start) / (float)(max)), 0, 1) * 2;
							LinePos.X = (float)(Math.Cos(angle) * -(index) + PlayerCenter.X);
							LinePos.Y = (float)(Math.Sin(angle) * -(index) + PlayerCenter.Y);
							Main.spriteBatch.Draw(Main.hbTexture1, LinePos, new Rectangle?(new Rectangle(4, 2, 4, Main.hbTexture1.Height-4)), Main.MouseBorderColor * alpha * Config.Client.Alpha, angle, new Vector2(6f, 4f), 0.5f, SpriteEffects.None, 0f);
						}
						LinePos.X = (float)(Math.Cos(angle) * -(distance - 2) + PlayerCenter.X);
						LinePos.Y = (float)(Math.Sin(angle) * -(distance - 2) + PlayerCenter.Y);
						Main.spriteBatch.Draw(Main.hbTexture1, LinePos, new Rectangle?(new Rectangle(0, 2, 4, Main.hbTexture1.Height-4)), Main.MouseBorderColor * alpha * Config.Client.Alpha, angle, new Vector2(6f, 4f), 0.5f, SpriteEffects.None, 0f);
					}
					for (int index = 0; index < distance - 2; index+=(int)(4 * 0.5f)) {
						alpha = MathHelper.Clamp(((float)(index - start) / (float)(max)), 0, 1) * 2;
						LinePos.X = (float)(Math.Cos(angle) * -(index) + PlayerCenter.X);
						LinePos.Y = (float)(Math.Sin(angle) * -(index) + PlayerCenter.Y);
						Main.spriteBatch.Draw(Main.hbTexture2, LinePos, new Rectangle?(new Rectangle(4, 4, 4, Main.hbTexture2.Height-8)), Main.cursorColor * alpha * Config.Client.Alpha, angle, new Vector2(6f, 2f), 0.5f, SpriteEffects.None, 0f);
					}
					LinePos.X = (float)(Math.Cos(angle) * -(distance - 2) + PlayerCenter.X);
					LinePos.Y = (float)(Math.Sin(angle) * -(distance - 2) + PlayerCenter.Y);
					Main.spriteBatch.Draw(Main.hbTexture2, LinePos, new Rectangle?(new Rectangle(0, 4, 4, Main.hbTexture2.Height-8)), Main.cursorColor * alpha * Config.Client.Alpha, angle, new Vector2(6f, 2f), 0.5f, SpriteEffects.None, 0f);
				}
				if (Config.Client.Method == 2) {
					if (Main.ThickMouse) {
						for (int index = 0; index < Main.screenHeight; index+=(int)((Main.hbTexture1.Width-8) * 0.5f)) {
							LinePos.X = Main.MouseWorld.X - Main.screenPosition.X;
							LinePos.Y = index;
							Main.spriteBatch.Draw(Main.hbTexture1, LinePos, new Rectangle?(new Rectangle(4, 2, Main.hbTexture1.Width-8, Main.hbTexture1.Height-4)), Main.MouseBorderColor * Config.Client.Alpha, (float)(Math.PI * 90f / 180.0), new Vector2(6f, 4f), 0.5f, SpriteEffects.None, 0f);
						}
						for (int index = 0; index < Main.screenWidth; index+=(int)((Main.hbTexture1.Width-8) * 0.5f)) {
							LinePos.X = index;
							LinePos.Y = Main.MouseWorld.Y - Main.screenPosition.Y;
							Main.spriteBatch.Draw(Main.hbTexture1, LinePos, new Rectangle?(new Rectangle(4, 2, Main.hbTexture1.Width-8, Main.hbTexture1.Height-4)), Main.MouseBorderColor * Config.Client.Alpha, (float)(Math.PI * 0f / 180.0), new Vector2(6f, 4f), 0.5f, SpriteEffects.None, 0f);
						}
					}
					for (int index = 0; index < Main.screenHeight; index+=(int)((Main.hbTexture2.Width-8) * 0.5f)) {
						LinePos.X = Main.MouseWorld.X - Main.screenPosition.X;
						LinePos.Y = index;
						Main.spriteBatch.Draw(Main.hbTexture2, LinePos, new Rectangle?(new Rectangle(4, 4, Main.hbTexture2.Width-8, Main.hbTexture2.Height-8)), Main.cursorColor * Config.Client.Alpha, (float)(Math.PI * 90f / 180.0), new Vector2(6f, 2f), 0.5f, SpriteEffects.None, 0f);
					}
					for (int index = 0; index < Main.screenWidth; index+=(int)((Main.hbTexture2.Width-8) * 0.5f)) {
						LinePos.X = index;
						LinePos.Y = Main.MouseWorld.Y - Main.screenPosition.Y;
						Main.spriteBatch.Draw(Main.hbTexture2, LinePos, new Rectangle?(new Rectangle(4, 4, Main.hbTexture2.Width-8, Main.hbTexture2.Height-8)), Main.cursorColor * Config.Client.Alpha, (float)(Math.PI * 0f / 180.0), new Vector2(6f, 2f), 0.5f, SpriteEffects.None, 0f);
					}
				}
				if (Config.Client.Method == 3) {
					distance = MathHelper.Clamp(distance * 0.5f, 0, max);
					LinePos.X = (float)(Math.Cos(angle) * -(distance) + PlayerCenter.X);
					LinePos.Y = (float)(Math.Sin(angle) * -(distance) + PlayerCenter.Y);
					float alpha = 1f;
					if (Main.ThickMouse) {
						alpha = MathHelper.Clamp(((float)(distance - start) / (float)(max)), 0, 1) * 0.5f;
						for (int index2 = 0; index2 < 4; ++index2) {
							Main.spriteBatch.Draw(Main.cursorTextures[11], LinePos, new Rectangle?(new Rectangle(0, 0, Main.cursorTextures[11].Width, Main.cursorTextures[11].Height)), Main.MouseBorderColor * alpha * Config.Client.Alpha, angle - (float)(Math.PI * 45f / 180.0), new Vector2(2, 2), 1f, SpriteEffects.None, 0f);
							Main.spriteBatch.Draw(Main.cursorTextures[11], LinePos, new Rectangle?(new Rectangle(0, 0, Main.cursorTextures[11].Width, Main.cursorTextures[11].Height)), Main.MouseBorderColor * alpha * Config.Client.Alpha, angle - (float)(Math.PI * 45f / 180.0), new Vector2(2, 0), 1f, SpriteEffects.None, 0f);
							Main.spriteBatch.Draw(Main.cursorTextures[11], LinePos, new Rectangle?(new Rectangle(0, 0, Main.cursorTextures[11].Width, Main.cursorTextures[11].Height)), Main.MouseBorderColor * alpha * Config.Client.Alpha, angle - (float)(Math.PI * 45f / 180.0), new Vector2(0, 2), 1f, SpriteEffects.None, 0f);
							Main.spriteBatch.Draw(Main.cursorTextures[11], LinePos, new Rectangle?(new Rectangle(0, 0, Main.cursorTextures[11].Width, Main.cursorTextures[11].Height)), Main.MouseBorderColor * alpha * Config.Client.Alpha, angle - (float)(Math.PI * 45f / 180.0), new Vector2(0, 0), 1f, SpriteEffects.None, 0f);
						}
					}
					alpha = MathHelper.Clamp(((float)(distance - start) / (float)(max)), 0, 1) * 2;
					Main.spriteBatch.Draw(Main.cursorTextures[0], new Vector2(LinePos.X + 2, LinePos.Y + 2), new Rectangle?(new Rectangle(0, 0, Main.cursorTextures[0].Width, Main.cursorTextures[0].Height)), new Color(0f, 0f, 0f, 0.2f) * alpha * Config.Client.Alpha, angle - (float)(Math.PI * 45f / 180.0), new Vector2(1, 1), 1f * 1.1f, SpriteEffects.None, 0f);
					Main.spriteBatch.Draw(Main.cursorTextures[0], LinePos, new Rectangle?(new Rectangle(0, 0, Main.cursorTextures[0].Width, Main.cursorTextures[0].Height)), Main.cursorColor * alpha * Config.Client.Alpha, angle - (float)(Math.PI * 45f / 180.0), new Vector2(0, 0), 1f, SpriteEffects.None, 0f);
				}
			}
		}
	}
}