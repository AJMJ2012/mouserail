﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System;
using Terraria.GameContent.Events;
using Terraria.ID;
using Terraria.IO;
using Terraria.ModLoader.IO;
using Terraria.ModLoader;
using Terraria.UI;
using Terraria;

namespace MouseRail {
	public class MouseRail : Mod {
		public MouseRail() {
			Properties = new ModProperties() {
				Autoload = true
			};
		}

		bool LoadedFKTModSettings = false;
		public override void Load() {
			if (Main.netMode == 2) { return; }
			Config.ReadConfig();
			LoadedFKTModSettings = ModLoader.GetMod("FKTModSettings") != null;
			if (LoadedFKTModSettings) {
				try { LoadModSettings(); }
				catch (Exception e) {
					DALib.Logger.ErrorLog("Unable to Load Mod Settings", Config.modPrefix);
					DALib.Logger.ErrorLog(e, Config.modPrefix);
				}
			}
		}

		public override void PostUpdateInput() {
			if (LoadedFKTModSettings && !Main.gameMenu && Main.netMode != 2) {
				if (DALib.DALib.tick % 60 == 0) {
					try {
						List<Type> Types = new List<Type>{ typeof(Config.Global) };
						if (Main.netMode != 2) { Types.Add(typeof(Config.Client)); }
						if (Main.netMode != 1) { Types.Add(typeof(Config.Server)); }
						string OldConfig = null;
						foreach (Type type in Types) { OldConfig += JsonConvert.SerializeObject((type.GetFields(BindingFlags.Static | BindingFlags.Public).Select(field => field.GetValue(type)))); }
						UpdateModSettings();
						string NewConfig = null;
						foreach (Type type in Types) { NewConfig += JsonConvert.SerializeObject((type.GetFields(BindingFlags.Static | BindingFlags.Public).Select(field => field.GetValue(type)))); }
						if (OldConfig != NewConfig) { Config.SaveConfig(); }
					}
					catch (Exception e) {
						DALib.Logger.ErrorLog("Unable to compare config data", Config.modPrefix);
						DALib.Logger.ErrorLog(e, Config.modPrefix);
					}
				}
			}
		}

		private void LoadModSettings() {
			FKTModSettings.ModSetting setting = FKTModSettings.ModSettingsAPI.CreateModSettingConfig(this);
			setting.EnableAutoConfig();
			setting.AddInt("Method", "Method", 0, 3, false);
			setting.AddFloat("Alpha", "Alpha", 0f, 1f, false);
		}

		private void UpdateModSettings() {
			FKTModSettings.ModSetting setting;
			if (FKTModSettings.ModSettingsAPI.TryGetModSetting(this, out setting))  {
				setting.Get("Method", ref Config.Client.Method);
				setting.Get("Alpha", ref Config.Client.Alpha);
			}
		}
	}

	public static class Config {
		public static class Global {}
		public static class Client {
			public static int Method = 3;
			public static float Alpha = 1f;
		}
		public static class Server {}

		public static string modName = "MouseRail";
		public static string modPrefix = "MR";
		private static Preferences Configuration = new Preferences(Path.Combine(Main.SavePath, "Mod Configs/" + modName + ".json"));

		public static void Load() {
			ReadConfig();
		}

		public static void ReadConfig() {
			if(Configuration.Load()) {
				Configuration.Get("Method", ref Client.Method);
				Configuration.Get("Alpha", ref Client.Alpha);
				DALib.Logger.DebugLog("Config Loaded", Config.modPrefix);
			}
			else {
				DALib.Logger.DebugLog("Creating Config", Config.modPrefix);
			}
			SaveConfig();
		}

		public static void ClampConfig() {
			Client.Method = (int)MathHelper.Clamp(Client.Method, 0, 3);
			Client.Alpha = MathHelper.Clamp(Client.Alpha, 0, 1);
		}

		public static void SaveConfig() {
			ClampConfig();
			Configuration.Clear();
			Configuration.Put("Method", Client.Method);
			Configuration.Put("Alpha", Client.Alpha);
			Configuration.Save();
			DALib.Logger.DebugLog("Config Saved", Config.modPrefix);
		}
	}

	public class Commands : ModCommand {
		public override CommandType Type {
			get { return CommandType.Chat; }
		}
		public override string Command {
			get { return Config.modPrefix.ToLower(); }
		}
		public override string Description {
			get { return mod.Name; }
		}
		public override string Usage {
			get { return Command + " reload"; }
		}
		public override void Action(CommandCaller caller, string input, string[] args) {
			switch (args[0].ToLower()) {
				case "reload":
					DALib.Logger.Log("Config Reloaded", Config.modPrefix);
					Config.ReadConfig();
					return;
			}
		}
	}
}
